$(document).ready(function(){
  $("#con").click(function(){
	
	$.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[789]\d{9}$/.test(value);
    }, "Phone numbers should contain 10 digits");
    
	$(".consultForm").validate({
  rules: {
    // simple rule, converted to {required:true}
	spec: "required",
    fname: "required",
    // compound rule
    email: {
      required: true,
      email: true
    },
	num:{
	 required: true,
	 loginRegex: true,
	}
  },
  messages: {
    fname: "Please specify your name",
    email: {
      required: "We need your email address to contact you",
      email: "Your email address must be in the format of name@domain.com"
    },
    num:{
      required: "We need your phone number to contact you",
      loginRegex:"Phone numbers should contain 10 digits"
    }
  }

});
  });

});