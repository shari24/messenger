/**
 * Created by Vaghula krishnan on 21-01-2015.
 */

'use strict';

var indexCtrl = require("../../web/index");
module.exports = {

	'/':{
        methods: ['get'],
        fn:[indexCtrl.main]
    },   
};