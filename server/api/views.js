var mysqlimpli=require('Mysqlimpli');

var view = function(req, res){
  res.render('index', { title: 'Home' });
};



var users = function(req, res){
    var queryString="select count(*) as count from user_details";
	mysqlimpli.execute(queryString,function(err,result) {
        if(err)
        {
        console.log(err);
        res.send({count:0});
        }
        else{
            //console.log(result[0].count);
            res.send({count:result[0].count});
        }
    });
};

var msgs = function(req, res){
    var queryString="select count(*) as count from conversation";
	mysqlimpli.execute(queryString,function(err,result) {
        if(err)
        {
        console.log(err);
        res.send({count:0});
        }
        else{
            //console.log(result[0].count);
            res.send({count:result[0].count});
        }
    });

};

exports.index=view;
exports.users=users;
exports.msgs=msgs;